+++
title = "Meet & Greet"
date = "2019-08-13"
subtitle = "Saturday, August 31 2019"
image = "img/Meet -N-Greet.png"
description = "This is meta description for the event"
tags = ["theme"]
categories = ["starting"]
+++

Expecting a baby soon? 
Interested in birthing classes? 
Thinking about hiring a doula? 

Then come spend some time with us on Saturday morning. It's a perfect opportunity to scope out your options!!

<!--more-->

## Event Date
 Saturday, August 31st

## Event Time
 10:00 am - 12:00 pm

## Location
 1423 Brierwood Pl SW
 Jacksonville, AL 36265

## Event Cost

+ **__FREE__**

## What To Expect
- * Labor Doula
- * Postpartum Doula
- * Bradley Birthing Instructor
- * Door Prize!
- * Lots of information

