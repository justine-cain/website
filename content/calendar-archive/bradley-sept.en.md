+++
title = "Bradley Birthing Method - 12 Week Class Series"
date = "2019-08-13"
subtitle = "September 5, 2019"
image = "img/workbook.jpg"
description = "This is meta description for the event"
tags = ["theme"]
categories = ["starting"]
+++


Join me as we take an in-depth look at pregnancy, labor, delivery, postpartum and breastfeeding. We will teach you about the stages of labor, techniques for relaxation, coaching strategies, and what to expect after delivery.

<!--more-->

## Class Dates
Thursday, September 5th - Thursday, December 19th

## Class Time
6:00 - 8:00 pm

## Place
My home in Jacksonville (address given after registration)

## Class Cost

+ **__$200 total__**
+ &nbsp;&nbsp;$30 deposit due upon registration
+ &nbsp;&nbsp;$170 due on first class (cash or check)

## What to Wear/Bring to Class
 - * Comfortable Clothes (for you and coach)
 - * Birthing Coach ❤
 - * 2 comfortable pillows
 - * Note taking material

