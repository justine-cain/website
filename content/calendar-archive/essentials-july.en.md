+++
title = "Natural Birth Essentials Class"
date = "2019-04-12T14:15:59-06:00"
subtitle = "Saturday, September 28 2019"
image = "img/notes.jpg"
description = "This is meta description for the event"
tags = ["theme"]
categories = ["starting"]
+++

Join me for an information packed class as we take a look into pregnancy, labor, delivery, postpartum and breastfeeding. We will teach you about the stages of labor, techniques for relaxation, coaching strategies, and what to expect after delivery.

<!--more-->

## Class Date
 Saturday, September 28th

## Class Time
 8:00 am - 2:00 pm

## Location
 My home in Jacksonville - address given after registration

## Class Cost

+ **__$150 total__**
+ &nbsp;&nbsp;$30 deposit due upon registration
+ &nbsp;&nbsp;$120 due on first class (cash or check)

## What to Wear/Bring to Class

 - * Comfortable Clothes (for you and coach)
 - * Birthing Coach ❤
 - * 2 comfortable pillows
 - * Note taking material

