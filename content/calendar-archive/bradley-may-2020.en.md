+++
title = "Bradley Birthing Method - 12 Week Class Series"
date = "2020-03-12"
subtitle = "May 07, 2020"
image = "img/workbook.jpg"
description = "This is meta description for the event"
tags = ["bradley", "birth"]
categories = ["starting"]
+++


Join me as we take an in-depth look at pregnancy, labor, delivery, postpartum and breastfeeding. We will teach you about the stages of labor, techniques for relaxation, coaching strategies, and what to expect after delivery.

<!--more-->

## Class Dates
Thursday, May 7th - Thursday, August 6th

## Class Time
6:00 - 8:00 pm

## Place
1423 Brierwood Pl SW  
Jacksonville, AL 36265

## Class Cost

+ **__$250 total__**
+ &nbsp;&nbsp;$30 deposit due upon registration (cash, check or PayPal)
+ &nbsp;&nbsp;$220 due on first class (cash, check or PayPal)

## What to Wear/Bring to Class
 - * Comfortable Clothes (for you and coach)
 - * Birthing Coach ❤
 - * 2 comfortable pillows
 - * Note taking material
