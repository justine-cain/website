+++
title = "What To Expect Here"
date = "2019-05-22T00:00:00+00:00"
description = "This is meta description for blog page"
tags = ["theme"]
categories = ["starting"]
+++


This blog is intended to be encouraging and thought provoking to those who are considering natural birth for themselves and for those who are simply thinking about the current state of pregnancy and birth in America, particularly the state of Alabama. I hope to address systemic topics and issues, as well as personal/individual topics that are common to all of us. There's much on the way!  
